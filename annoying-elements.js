// ==UserScript==
// @name         Alert production production
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  just annoying jumping elements on page
// @author       david.sanda
// @match        https://*somedomain.com/*
// @match        https://*somedomain.eu/*
// @grant        none
// ==/UserScript==

(function() {

    function AnnoyingElements(numberOfAnnoyingElements) {

        numberOfAnnoyingElements = numberOfAnnoyingElements || getRandomNumber(8, 20);

        var elements = [];

        function getRandomNumber(min, max) {
            min = Math.ceil(min);
            max = Math.floor(max);

            return Math.floor(Math.random() * (max - min)) + min;
        }

        function getRandomColor(){
            return '#' + Math.random().toString(16).substr(-6);
        }

        function createElement(elementColor) {
            var elm = document.createElement('div');
            elm.setAttribute(
                'style',
                'top:0;left:0;background: ' + elementColor + ';width:15%;height:15%;position:absolute;z-index: 99999;color:' + getRandomColor() + ';display: flex;justify-content: center; align-items: center;font-weight:bold;font-size:20px;transition:left .3s linear, top .3s linear'
            );
            elm.innerHTML = 'CATCH ME!<br>:P';

            function AnnoyingElement(elm) {

                var interval = null;

                function destroy(event) {
                    clearInterval(interval);
                    event.target.remove();
                }

                function jump() {
                    var scrollTop = document.body.scrollTop,
                        scrollLeft = document.body.scrollLeft,
                        elementWidth = elm.clientWidth,
                        elementHeight = elm.clientHeight;

                    elm.style.top = getRandomNumber(scrollTop, scrollTop + window.innerHeight - elementHeight) + 'px';
                    elm.style.left = getRandomNumber(scrollLeft, scrollLeft + window.innerWidth - elementWidth) + 'px';
                    
                    clearInterval(interval);
                    startJumping();
                }

                function startJumping() {
                    interval = setInterval(jump, getRandomNumber(3, 12) * 100);

                    return this;
                }

                return {
                    insertToPage: function () {
                        document.body.prepend(elm);

                        return this;
                    },

                    closable: function () {
                        elm.addEventListener('click', destroy);

                        return this;
                    },

                    startJumping: function () {
                        jump();
                        startJumping();
                    }
                };
            }

            return new AnnoyingElement(elm);
        }

        return {
            init: function () {
                for (var i = 0; i < numberOfAnnoyingElements; i++) {
                    elements.push(
                        createElement(getRandomColor())
                    );
                }

                var initInterval = setInterval(
                    function () {
                        var elm = elements.pop();
                        if (elm) {
                            elm.insertToPage().closable().startJumping(); // and fun begins :D
                        } else {
                            clearInterval(initInterval);
                        }
                    },
                    getRandomNumber(1, 10) * 100
                );
            }
        };
    }

    (new AnnoyingElements()).init();

})();

